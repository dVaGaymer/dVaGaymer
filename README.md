## [![](https://img.shields.io/badge/C-as%20in%20I%20dont%20need%20Classes-ff69b4?style=for-the-badge&logo=C)](https://www.gnu.org/software/libc/manual/html_node/index.html)

[![](https://badgen.net/badge/icon/terminal/black?icon=terminal&label)](https://al7aro.com)
[![](https://badgen.net/badge/icon/git/orange?icon=git&label)](https://github.com/dVaGaymer)
[![](https://img.shields.io/badge/42-student-blueviolet)](https://profile.intra.42.fr/users/alopez-g)


```c
void	_program(void){}
void	_focus(void){}
void	_listen(void){}
void	_organize(void){}

typedef struct	s_lowleveldev
{
	char	name[8];
	char	interests[8][16];
	char	lang[8][8];
	void	*skills[8];
}		t_lowleveldev;

int	main(void)
{
	t_lowleveldev	me =
	{
		{"AlVARO"},
		{"C", "ASM", "OPENGL", "PHYSICS", "MATH", "VIDEOGAMES", "RUBIK CUBES"},
		{"en_US", "es_ES"},
		{_program, _focus, _listen, _organize}
	};
	return (0);
}
```

## I like this GIF
![Alt Text](https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/5eeea355389655.59822ff824b72.gif)


## 24/7 Available via
[![](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto:al7aro@gmail.com)
[![](https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/dVaGaymer)
[![](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/%C3%A1lvaro-l%C3%B3pez-g%C3%B3mez-1a2765193/)
